結束了！
突然結束了！
但是，哈特結束了他的休息時間。

從，
姐姐出現了！
姐姐溫柔地微笑著。

這是剛剛發生的事情的概要。在那之前被眼鏡矮子捉住的我。
總覺得混亂起來了。

直到剛才為止被我埋伏的麗莎不敵地笑了。

「聽說了。聽說他打敗了哈芬家的少爺。不愧是贏了我的男人。」

「他最近的品行實在讓人看不下去。如果能反省後變得老實就好了。」

「哈特君贏過王子嗎？在今年新生中，你可是擁有超越王子的實力。」

「那是小時候的事情。」

實際上，我從那時開始就沒怎麼變強。只有結界魔法的使用稍微變好了。

四周人聲嘈雜。
似乎是對萊斯的話做出了反應。不只是剛才的寒暄程度的交談，用實際的比賽我勝了王子好像相當的衝擊。
不要把這個話題扯大。因此，轉換話題。

「您找我有什麼事嗎？」

「喂喂，同樣是新生，別用禮貌的語氣說話。」

「不，我在問公主。」

「什麼呀！」

「不要大聲說話。是的，我找你有事。哈特，你已經決定要加入了嗎？」

「不，還沒有。」

「好！那就和我一樣吧。雖然很嚴厲，但教練室裡卻鍛鍊得很好。」

「不，和我進同一個研究室吧。能夠進行最尖端的魔法研究，是非常有意義的地方。」

「等一下。最初注意到他的是我。即使是王家的人，這裡也不能讓步。或者說，在學校內擺弄權力的話，會讓人很困擾。」

萊斯悄悄地走了過來。太大隻太礙事了。

「喂，哈特，這小子是誰？」

「唔，這麼小真是失禮啊。我是提亞利埃塔．路塞亞納爾。是這個學院的教授。擅長古代魔法。」

「喔，喔……失禮了。但是和最初沒有關係吧。就算是老師也不會客氣的。」

真嚇人啊。那個自大的萊斯，暫時敬畏著。成長了呢。哥哥我既不高興也不傷心。沒興趣。

「突然想起來了。」

萊斯對我耳語。

「路塞亞納爾教授，她有很多問題。做著不能出好成果的陳舊的研究，好像與其他的教師也一個勁地起衝突。我不會說壞話。就到這了。」

「王子殿下，我聽到囉。結果不是沒出來，只是愚蠢的人不能理解。」

「喂，就是這種感覺啊。」

「什麼啊！你這混蛋！」

安慰著怒氣衝衝的矮子教授的是我的姐姐。

「話雖如此，我對路塞亞納爾教授很抱歉，但這裡我也不能讓步。」

「提亞也沒關係，公主。但是不行。這孩子是我的。」

「不，請跟我一起來。」

不知怎麼的，對我的競爭開始了。
而且，還有。

「那個孩子還沒決定所屬部門。」
「我們也去嗎？」
「但是在公主和王子之間呢。」
「有些不對勁，沒關係。」

這樣下去會收拾不完的。
這個遠離這個地方比較好。

「「順便。」」

突然三人聚在一起是什麼？

「從剛才開始就在我們周圍。」
「轉來轉去的白髮奇妙的女人。」
「你認識哈特嗎？」

啊，我也開始在意了。男裝的白髮馬尾辮女。

「嗯，我突然想到了。她是筆試成績第一的平民孩子。雖然實際技巧簡單，綜合來說比平均稍高，不過，古代魔法也很熟悉，所以還在檢查著。我差點忘掉哈特。嗯……名字是什麼呢……？」

您很快就忘了名字嗎？話說，如果檢查過了的話，不是找我，而是勸誘那傢伙吧。

「其實我和那傢伙有約定哦。」

嗯，這裡可以利用那個女人嗎？

「還有，我屬於哪裡，由我來決定。請不要再邀請我了。」

直截了當地說，我衝刺。抓住了白髮馬尾辮女的胳膊。

「拒絕了王子和公主的邀請。」
「我不會說得那麼清楚。」
「我做不到。」
「太厲害了。」

一個一個給我驚訝。在我看來是同年齡的親戚的孩子們。實際上是姊弟。

「啊，等一下，哈特君。喂，幹什麼！」

「你差不多該放棄了。」

萊斯好。就那樣把眼鏡矮子教授的翅膀預先勒緊。

「嗚，我不會放棄的！」

我一邊聽著這樣的話，一邊拉著白髮馬尾辮女——。



走到校內樹林深處。這所學校真大。

「先說聲謝謝，得救了。但妳已經沒事了。不管去哪裡都行。」

「雖然完全不能理解狀況，但因為是難得的機會，所以想和你談談。」

這真是個不走運的女人啊。

「只是，我現在有點困惑。無法理解的程度，你是最大的。」

「嗯？我？」

下一個瞬間，我覺得她又要說些奇怪的話了。

「啊。現在的你，和昨天的你是同一個人嗎？」

我在裡面，打開了什麼開關。
構築半徑十米的半球狀結界。從外邊看不見在光學迷彩後的我們，聲音也不會外泄。是不允許逃出和入侵的牢固的牢籠。

女人的臉繃緊了。向左右或向上移動眼睛。

「結界嗎？太棒了。」

「……你看得到嗎？」

我的警戒等級更上一層。我的結界被用眼捉住的，前後也只有閃光公主。雖然沒有向本人確認。
不管怎麼說，這傢伙是——。

「沒有。不愧是我無法看到的如此完美結界。雖然和肌膚感覺有些不同，但也只是【感覺】而已。如果不集中注意力到極限的話，就會忽略了吧。」

儘管如此，比起魔族的芙蕾和麗莎更清楚。
直覺很準嗎？所以能和我分辨出分身嗎？不知道。我不知道，問問看吧。

「我和昨天有什麼不同嗎？」

「誒？啊，剛才的話啊。如果我的發言讓你不高興的話就先道歉吧。但是，從昨天的你那裡完全感受不到魔力，今天的你卻有著幾乎要讓人被吹跑的【魔力壓】。」

「外表有差嗎？」

「外表沒改變。所以覺得不可思議。」

嗯。的確分身是沒有魔力的。所以魔法也不能使用。但是我的魔力說是『魔力壓』，就只有芙蕾或麗莎吧。啊，還有召喚獸們。

「你是魔族嗎？」

「雖然很想知道你索這個想法的起源，但是還是放棄吧。我是人類。如果不是，那就麻煩了。」

別說得這麼含糊，這傢伙。

「算了。你找我有事吧？什麼事？」

首先關閉什麼開關。結界保持原樣。因為要是被眼鏡矮子博士捉住了不得了。

這女人（沒聽說過名字。事到如今，我連問都沒問。）直直地看著我說。

「感謝和道歉，並傳達我的請求。」

真多啊。

「首先要從道歉開始。對不起，昨天把你捲入了麻煩。那時候雖然道歉了，但之後你也被那個高年級生攻擊了。甚至被要求決鬥。是我沒能做出合適行動的結果。真的很抱歉。」

女人深深地低下了頭。花很多時間抬起頭，

「在他放出魔法之前，將我拉到一旁，保護著我免受攻擊。很抱歉無法推測你的意圖，但是從結果來看，你救了我。多謝了。」

完全沒有那樣的打算？是分身覺得太礙事，只好推開她了。

「然後，關於最後的請求……」

女人困惑地把眉毛弄成八字沉默著。不久，她就像下定決心似的大聲喊了起來。