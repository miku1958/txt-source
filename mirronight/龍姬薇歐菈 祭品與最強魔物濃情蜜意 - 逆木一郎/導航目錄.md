# CONTENTS

龍姬薇歐菈 祭品與最強魔物濃情蜜意 - 逆木一郎  
竜姫のヴィオラ 生贄は最強の魔物と恋に落ちて  
龍姬薇歐菈 祭品與最強魔物濃情蜜意  
竜姫のヴィオラ  

作者： 逆木一郎  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%BE%8D%E5%A7%AC%E8%96%87%E6%AD%90%E8%8F%88%20%E7%A5%AD%E5%93%81%E8%88%87%E6%9C%80%E5%BC%B7%E9%AD%94%E7%89%A9%E6%BF%83%E6%83%85%E8%9C%9C%E6%84%8F%20-%20%E9%80%86%E6%9C%A8%E4%B8%80%E9%83%8E.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/mirronight/%E9%BE%8D%E5%A7%AC%E8%96%87%E6%AD%90%E8%8F%88%20%E7%A5%AD%E5%93%81%E8%88%87%E6%9C%80%E5%BC%B7%E9%AD%94%E7%89%A9%E6%BF%83%E6%83%85%E8%9C%9C%E6%84%8F.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/mirronight/out/%E9%BE%8D%E5%A7%AC%E8%96%87%E6%AD%90%E8%8F%88%20%E7%A5%AD%E5%93%81%E8%88%87%E6%9C%80%E5%BC%B7%E9%AD%94%E7%89%A9%E6%BF%83%E6%83%85%E8%9C%9C%E6%84%8F.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/mirronight/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/mirronight/龍姬薇歐菈%20祭品與最強魔物濃情蜜意%20-%20逆木一郎/導航目錄.md "導航目錄")




## [contents](contents)

- [第一章_龍姬的蛻變](contents/00000_%E7%AC%AC%E4%B8%80%E7%AB%A0_%E9%BE%8D%E5%A7%AC%E7%9A%84%E8%9B%BB%E8%AE%8A.txt)
- [第二章_勇者的真相](contents/00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0_%E5%8B%87%E8%80%85%E7%9A%84%E7%9C%9F%E7%9B%B8.txt)
- [第三章_龍姬的寵愛](contents/00020_%E7%AC%AC%E4%B8%89%E7%AB%A0_%E9%BE%8D%E5%A7%AC%E7%9A%84%E5%AF%B5%E6%84%9B.txt)
- [第四章_邪龍來襲](contents/00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0_%E9%82%AA%E9%BE%8D%E4%BE%86%E8%A5%B2.txt)

