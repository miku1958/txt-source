１６

「異世界的迷宮深層的冒険者之間的死鬥嗎。猛烈到難以想像存在於這世上啊」

雷肯什麼都沒說。光是從原本的世界來到這世界就會增強能力增加壽命的樣子，在這世界的迷宮打倒魔獸的話，力量的成長率似乎也很高。然後在雷肯的場合，還能兼有原本世界的技能和這世界的魔法。無意說出這秘密。而且不管怎麼說，佐爾坦的強大都是特別的。

基爾安特向雷肯提出質問。

「在百二十階層，有了怎樣的戰鬥」
「我跟阿利歐斯兩人，先進了普通個體的房間的侵入通路。然後房間裡就湧現了兩隻魔獸」

伊萊莎插了嘴。

「等等。兩隻？」
「要從那邊嗎。從百階層開始不會湧現〈黑肌白幽鬼〉或〈赤肌白幽鬼〉，而是〈鐵甲白幽鬼〉」
「這種事當然知道」
「〈鐵甲〉最多只會出現五隻」
「就說了這種事當然知道了」
「伊萊莎。閉上嘴聽雷肯說」
「是」
「房間裡湧現的〈鐵甲〉的數量會跟進入侵入通路的人數相同，有這樣的構造，而那最大數量是五隻」
「誒？」
「進入侵入通路的是兩人的話，房間裡就會湧出兩隻〈鐵甲〉」

基爾安特、海丹特和伊萊莎都沉默了。

「到達百階層後，我們跟之前共同探索的〈遊手好閒〉分別，以兩人來探索。因此，從百階層到百二十階層都是以兩隻為對手來戰鬥」
「等等，雷肯。進了兩人就只會出現兩隻〈鐵甲〉。湧出的〈鐵甲〉的數量跟進入侵入通路的人數相同」
「對」
「這種事」

這件事在昨晚被許多冒険者得知了，而〈遊手好閒〉在那之前就知道了。傳聞應該很快就會傳遍城鎮，已經沒有保密的意義了。

「懷疑的話就驗證吧。總之我們到達了百二十階層。當時，我注意到了，跟下來的階梯不同的階梯，存在於階層最深處的房間，也就是〈守護者〉的房間裡」
「百二十階層的〈守護者〉的房間沒有階梯」
「伊萊莎。說過住嘴了吧」
「然而實際進入房間後，看不到應該會有的階梯。所以覺得，不滿足某種條件，階梯就不會出現」
「喔。有趣。你的特殊感知技術，捕捉到了那階梯嗎」
「對。然後那階梯很窄。所以就覺得是不是一人用的階梯。我就獨自進了侵入通路。不出所料，出現的魔獸是一隻。然後把它打倒後就出現了向下的階梯。我轉移到原本的階梯告訴阿利歐斯這發現，阿利歐斯也獨自進了房間。我們就這樣到達了百二十一階層」

好一陣子都沒有人說話。打破沉默的是基爾安特。

「然後打倒了百二十一階層的〈守護者〉，得到了〈彗星斬〉嗎」
「不，不是那樣。不是〈守護者〉，是普通個體。我跟阿利歐斯都還沒打倒百二十一階層的〈守護者〉」
「原來如此。吾等迷宮竟然有這種秘密」
「兄長」
「怎麼了，海丹特」
「說不定，不在百二十一階層以下就不會出現〈彗星斬〉」
「老夫也這麼認為。如果是這樣，就能說明這一百一十六年來，為何都沒出現過〈彗星斬〉」
「伯父大人。馬上讓人探索百二十階層吧！」
「沒辦法吧。能探索百階層以下的隊伍，只剩下三組。然後那三組，都只有探索到百十階層」
「那麼就請雷肯殿」
「住嘴」

低沉的聲音。但是，是有讓人麻痺之魄力的聲音。被告誡的伊萊莎說不出話來。

「雷肯。這迷宮有到幾階層呢」
「不知道」
「是嗎，不知道嗎」

基爾安特閉上了眼，思索著什麼一陣子。不久後，張開眼睛並轉頭。

「拜亞德」
「是」
「讓店的店主過來吧」
「是」

納可被叫到侯爵面前，回應侯爵的請求，述說了佐爾坦的回憶。納可有聽祖父和父親說過很多事情。從開始到講完，花了不少時間。

一開始雖然說得很僵硬，但在述說途中漸漸熟練，道出了半開玩笑的有趣又讓人印象深刻的回憶。
基爾安特和海丹特都很能喝，笑得很開。

「啊啊，真開心。挺讓人放鬆的。差不多該回去了。雷肯」
「怎麼」
「能讓老夫看看〈彗星斬〉嗎」

基爾安特的拜託方式很直接。沒有強烈的慾望，也沒有精打細算的尖銳，是很柔和的說法。

雷肯拔出了〈彗星斬〉。
然後注入魔力。
形成了兩倍長的魔法刃。

「喔喔」

四人的眼睛都盯住了。
又注入了魔力。魔法刃伸長到了最大限度的五倍長。

「喔喔喔喔！」
「這真是」
「真美」
「好厲害」

雷肯停止了注入。魔法刃立刻就消失了。

「看了好東西了。謝謝」

基爾安特以眼神指示，騎士拜亞德便以料理和酒的謝禮為由，把裝了錢的袋子交給納可。

「那麼，失禮了」

只這麼說，基爾安特就走向了門。
拜亞德和海丹特也跟上了。伊萊莎要跟上時回到了桌子。

「雷肯店。來到這世界時，是掉在帕爾希魔嗎？」
「不是」

給了無法接受的表情，伊萊莎跟著伯父和父親走向出入口。

（侯爵他）
（毫無強迫之意呢）
（然後也毫無奪取之意）

不可能對〈彗星斬〉沒興趣。應該想要這把劍想得不得了才對。應該也還想知道更多迷宮的情報才對。但是，一絲慾望都沒有展現出來。也不以利益當誘餌。

了不起，雷肯想著。
然後那風格，就連雷肯也感覺被壓倒了。

覺得，茨波魯特侯爵基爾安特，正是做為貴族達到了〈剛劍〉之領域的男人。

（雖然拿來比較不好）
（但比沃卡的領主高了好幾段）

門被打開，以為回去了的伊萊莎現了身。

「雷肯殿」
「怎樣」
「前陣子，我跟佐爾爺過來了」
「啊啊」
「在那一天之後，佐爾爺的心情非常好」
「⋯喔」
「佐爾爺一直都沒有精神」
「是嗎」
「所以看到了取回精神的姿態，我跟父親大人還有伯父大人，都很開心」
「這樣啊」
「雷肯殿。謝謝你」

伊萊莎說完就關上了門。


飄來了一點花香似的香氣。