在監獄貝拉的大門前，薇斯塔莉努朝著我嘟著嘴。無論花費多少言語也無法表達的幾種感情，似乎都在她的表情上並存著。

她性格坦率，為人正直，但現在卻似乎走向了壞的方向。

“……你那不是決鬥，是壓根沒有勝算的胡來，這樣只會白白浪費生命。難道你身體上的傷口已經痊癒了嗎？”
她的嘴總算說出話來了。聲色裡似乎充滿了不滿和憤怒。
聽起來很嚴厲。指揮官說要把自己的生命暴露在敵人面前，為這邊爭取時間，所以給他一到兩個鼓勵不是很好嗎？

那個行為是好是壞姑且不論。什麼呀，已經習慣將性命暴露在危險之中了。希望你能對此放心。
雖然之後薇斯塔莉努還說了好些話，但似乎還是不足以說服我。恐怕無論說什麼也沒辦法對抗現在的局面吧。
但是，很遺憾，我也不能繼續聽下去了。如果可以的話，我也想繼續等待援軍。
“血已經止住了。對我來說已經足夠了”
一邊從深綠的軍服上拭去雪花，一邊用手指輕輕地撫摸著滲出血液的地方。傷口本身還確實存在著，但至少不再流血了。這個異常令人毛骨悚然,不過，同時內心也變強了。我到底還能不能回復到正經的身體呢？

冷空氣進入鼻孔，喉嚨微微地鳴叫，我輕輕地握住手指。
雖然嘴上說得很好，但是實際上體力還是不夠，有身體的大部分已經失去了的感覺。
到底之後能行動到什麼程度呢？
現在這個時候，我連一隻手的富餘都沒有。不，沒有富餘已經是我的日常了，是比以往任何時候，面對任何情況時都不夠。
以這種狀態，和那個女人敵對嗎？這麼一想，恐怖的感覺就從腳後跟深處涌了上來。

對薇斯塔莉努，同時也是對自己說，為之張開了口。
“這是戰爭。等我傷口痊癒，不可能跟敵人這麼說的。任何時候有敵人的話，那就只能做自己應該做的事了。”

如果是僱傭兵的公主應該知道這一點吧，繼續這樣說。薇斯塔莉努將嘴唇拉開，像是瞪著我似的抬起眼角說道。我收到的話語被死雪的寒氣籠罩著，變得模糊。

“如果你死了，我會怨恨你的。和姐姐一起，極度地怨恨你，持續一生的。”

奇怪，那是混雜了熱情的聲音。饒了我吧，你不是卡麗婭和芙拉朵吧。她們引起的騷動已經足夠了。
沒有回答薇斯塔莉努那充滿威脅的話語，只是向後輕輕地揮動手臂向前走去。

什麼呀，如果是她的話，以後肯定能把事情幹好的。我十分清楚她是一個比我聰明得多、優秀得多的人。
是作為貴族進行教育的緣故吧。在軍事統率這個方面，薇斯塔莉努有作為傭兵首領甚至稍顯浪費的行動力和知識。
正因為如此，即使我在敵將面前迎來愚蠢的終結，她也會很好地善後的。我相信如此。

腰部搖晃的紫電寶劍，似乎發出了聲音在蠢動著。那個樣子好像預感到了什麼。
監獄的大門咯吱咯吱地打開。在視線盡頭，死雪覆蓋的雪白中，站著那個女人。
從馬上下來等待這邊的身姿，是到哪裡都堂堂正正的英雄本身。以群青色為基調的魔術鎧甲，已經清晰可見。

當然是那樣。那是不折不扣的英雄。曾經的時候，正因為有她的存在，加萊斯特王國才能長久地維持自己的呼吸。
――瓦蕾麗·布萊特尼斯。曾經被魔人殺掉的女人。只有魔人能殺死的女人。

如果可能的話，不會想與她敵對。與其這樣說，不如說是連做夢都沒想到要與之敵對的對手。不用考慮，其在戰場的經驗和技能，全部都是超過我的吧。
但是，雖說如此，也不能輕易地敗北。那是對相信我的人的侮辱。這是向我曾經跨越的人吐痰的行為。

啊，這一點很討厭。不管拋開什麼，我都很討厭。

那句話，在我內心深處回蕩著。

◇◆◇◆

那個一對一的決鬥，沒有宣告開始的言詞。

我想恐怕是二人站在那裡的時刻，就彼此明白那就是信號了。無需說什麼，我拔出了寶劍，瓦蕾麗使用著魔術鎧甲，閃耀著能刺穿身體的眼神。

接著，下一個瞬間，群青色的魔術鎧甲在白雪之海中奔跑。同時，擁有明確的殺意所形成的力量讓物體搖動起來。

作為守護者的瓦蕾麗使用的武器不是鐵劍，其中一個原因是那不適合以多數魔物為對手。話雖如此，她的武器既不是騎士擅長的騎槍，也不是戰斧，更不是暗器之類的東西。這些武器在魔物群前，顯得過於纖細了。

當然，如果必要的話，她多少也可以使用。但現在不同。
瓦蕾麗使用的武器只有一個，她的魔術鎧甲本身。所以她實際使用的是自己的身體。
本來鎧甲不是武具，只是保護人類脆弱的皮膚的裝甲而已。但是聽說那個魔術鎧甲是個例外。
雖然不太清楚到底是怎麼回事，但它要比所有武器都優秀，能戰勝所有防具。當然，一切不過是傳聞而已。

但是，即便如此，她那粗魯的武裝做了什麼，還是很容易想像的。
將魔獸從頭到背，全部粉碎地殺掉。如果是這樣的話，那個魔術鎧甲有可能做到吧。
然後現在，那個愚直的殺意和武威乘著瓦蕾麗的右拳，威脅著要破壞我的生命。
背脊上有迅速升起翻涌的恐怖感。甚至讓人覺得被刀子戳還比較好。
反射性地驅動腰部，使用腳腕，讓寶劍的軌道與對方的拳頭重合。這是為了彈開瓦蕾麗的拳頭，然後就那樣斬下她的脖頸的一擊。紫電在呻吟的同時劃出一條線，撲向群青。

應該描繪的道路清晰可見。在眼前，寶劍和魔術鎧甲接合了。
同時，空間裡響起了巨大的聲音。那是無可爭辯的力量與力量的衝突音。
雪地上飛舞著火花，白色中閃耀著色彩。如此反覆數次。
寶劍無法擋開敵人的拳頭。不，豈止如此，對方的拳頭完全接下了寶劍，連進入對方的防守空隙都做不到。

脖子泛起雞皮疙瘩。照這樣下去，會死的。為了重整態勢，我一瞬間拔出了劍尖。然後以像要踢飛對手的勢頭猛地往背後跳去。
但是就在那時，瓦蕾麗轉動腰部發出了第二擊。它就像是向著稍許後退的我，打出暴風一般，攻了出去。
瞬間，天空爆炸了。臉頰邊緣的肉被刮落了，血和肉灑滿了死雪。牙齒麻木似地痙攣。如果再晚一步將拳偏離，那個被切開的就不是我的臉而是我的頭了吧。
但是，我連稍許感到安心的時間都沒被給予，瓦萊利似乎有不給予對方猶豫時間的性格。

還沒有緩過呼吸，第三擊就已經攻過來了。我怎麼也無法躲開，也沒辦法調整受衝擊時的姿勢。然後瓦蕾麗的右拳就明確地瞄準著我的要害。
她的動作全都很巧妙，而且速度很快，一切都充滿了令人窒息的洗練的精華。恐怕我只不過是被她隨心所欲地擺佈著罷了。

我重新理解到，我無論從身體上還是技術上都無法撐過長期戰。那樣的話我一定會敗北的。

正因為如此，只有用接下來的一擊砍掉對方的首級了。所謂的我的勝機就是那連一秒也不到的那一瞬間。

什麼呀，那就已經足夠了。

反射性地轉動腰部，扭緊雙臂。然後握緊寶劍的柄，橫擊瓦蕾麗的右拳，想將其打飛。
同時，全身的肌肉也在尖叫中躍動著，骨頭在戰場上發出了掩蓋不住的悲鳴。
啊，沒關係。如果能打敗瓦蕾麗的拳頭，即使我的全身被打碎那也是有餘的功績。
在已經連感覺都失去的指尖上注入力量，用渾身的力量握住寶劍。就這樣，沒有任何呼吸，紫電如波浪襲來。

雖說是魔術鎧甲，但既然是鎧甲就一定有接縫。脖子就是其中之一。因為是接合部，所以無法有很堅固的構造。那麼打碎了就肯定能殺掉，可以砍下對方的首級。

與此同時，與我的意志相重疊，的確能聽得到那個聲音。
——在這裡，去死吧。祖國的敵人。

耳朵裡，傳來風聲。