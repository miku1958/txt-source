龍乃是至高無上之種族。
先不管那種說法是否真實，反正龍很迷信那一套。
並且，那些至高種族的龍，並不會因為顧慮莎麗兒便不會對現狀抱持不滿。
就像過去的邱列。
與此同時，龍可不是在做慈善事業，是帶有目的才盤踞在這個星球上。
因此無法對採取與其目的不相符的行動的人類坐視不理。
即便是和莎麗兒作對也一樣。

「於是，你要負責阻止嗎」
「是的」

直接給對方回以凝視的莎麗兒，和似乎因尷尬而移開視線的邱列。
地點是孤兒院的接待室。
邱列也常常在孤兒院露面，通常不去接待室就直接進去了。
然而，察覺到險惡氣氛的莎麗兒，帶邱列去了接待室。
即便不是莎麗兒，邱列那古怪的模樣也一目了然。
舉止可疑到了那種地步，哪怕是遲鈍如莎麗兒也能察覺到一點蛛絲馬跡。
然後，半帶審問性質地，從邱列口中問出了龍的動向。

「於是你打算怎麼做？」
「當然是去阻止。畢竟那是我的使命」

莎麗兒立馬便回答了邱列的提問。
如往常一樣。

「那真的是你的使命嗎？」

但是，對邱列接下來的問題，莎麗兒卻無法立刻作答。

「現在人類在做什麼事，你也不是沒有理解。如果說你的使命是保護原生生物，那麼阻止要破壞這個星球的人類，才是你應有的行動才對吧？」

邱列抬起頭，像是下定決心般說出這番話。

「當然啦，因此就要根除人類，我覺得也是太過分了。但是，總歸要有點表示。若那真的是你的使命的話」

邱列猶如試探的視線。
莎麗兒沒有移開目光。
不過，也沒有說話。

莎麗兒她自己也知道自己的行動並不是最好選擇。
然而，她對此從未有過疑問。
莎麗兒是迷惘天使。
只為忠實地恪守使命，仿彿沒有自我意志的裝置。
更正確地說，只為了恪守使命，因此才無法以自己的意志去判斷事物。
裝置就該有個裝置樣，只按照預先設定的必要項目去做即可。
即使預先設定的必要項目出現bug也照樣執行不誤。
裝置本身不會對此抱有疑問。
莎麗兒也是同樣，雖然能理解自己的行動並非最佳選擇，然而卻並未對此抱有疑問。

但是，現在被邱列問到，莎麗兒開始思考起來。
這樣真的就好嗎？
那對莎麗兒來說，是第一次抱有疑問。
至今為止，莎麗兒一直在思考，在嘗試。
不過，那些在旁人看來，無非就是在逃避她原本的使命罷了。
莎麗兒原本的使命，是在諸神的手上保護這顆星球上的原住生物。
只要令神無法對他們下手即可，不需要做多餘的事。
雖然是這樣，但莎麗兒卻率先干涉了人類。
作為神的莎麗兒。
在那一刻起就相當於是她自己放棄了使命。
然而，莎麗兒對自己正確履行使命深信不疑。
此時，邱列給她投來問題。
思考歸思考，一直以來都沒有疑問之心的莎麗兒，此時才第一次對自己的行為感覺到有疑問。

「莎麗兒。你也差不多是時候擺脫使命的束縛了吧？你只要隨性而活即可。忘記他媽的使命，按照你自己的想法隨性而活即可」

邱列的話令莎麗兒很不能理解。
雖然能理解話中的意思，但她不曉得什麼叫做隨性而活。
對莎麗兒來說所謂活著就是完成使命，在這其中並沒有好惡之感受。
不，即使有好惡之感受，她也不理解。
並不是沒有感受。
但是，她並沒有理解自己事實上已經感受過了。
結果就是那些好惡一直都被她無視。

「我……不是很懂」
「我想也是」

邱列此時也並不覺得光靠語言就能動搖莎麗兒那顆固執的心。
但是，從莎麗兒的態度可以看出，結果比預想的要好。

「不過，我要做的事不會變」

假如有哪裡估計錯誤，那就是莎麗兒發自內心深處的願望，與阻止龍這件事一致。
邱列那句要她隨性而活的話，確實地打動了莎麗兒的心靈。
正因為被打動，才將生出來的些微疑問吹散。
因為莎麗兒想要做的，是「想要保護所有人」這種願望。
就是那種願望把「這真的是履行使命最好的選擇嗎」這個疑問吹散了。

「等等！」
「邱列。我自作主張把你當成了我的朋友。所以，請你別逼我親手殺死你」

邱列被那句話嚇到了。
意思是說就算把邱列稱作朋友，但敢來礙事的話就會殺了他。
然後，丟下原地石化的邱列，莎麗兒離開了接待室。

「我出門的時候，孤兒院的孩子們，就拜託你了」

走的時候，還留下了這樣一個任性的願望。
邱列無言注視著關上的門。
臉上是一副無可奈何的表情。
作為龍，作為莎麗兒的朋友，也作為對莎麗兒傾心的男人。
應該採取何種行動才好，邱列無法做出選擇。
若是作為龍行動的話，賭上性命攔住莎麗兒就是正確的。
即便莎麗兒和邱列之間擁有壓倒性的力量差距，不過拖一下時間還是可以的。
比方說，拿孩子來當人質。
然而，當他默默地目送莎麗兒出門的時候，已經不能那樣做了。
那麼，應當陪莎麗兒一起去嗎？
那樣做的話就等於是背叛了龍。
至今都以身而為龍而驕傲地活著的邱列，沒有選擇那個答案。
結果，邱列甚麼都沒做，能做到的就只有極為半途而廢的，什麼都不選擇。




在世界各地都同時發生了龍發起的大規模襲擊。
本應該在一瞬之間給予人類沉重的打擊。
不過，莎麗兒早早便加入戰局。
而且更重要的是，使用MA能量，波狄瑪斯設計的無數兵器幫助人類拼死抵抗，戰事往超乎龍預想的膠著化發展。
龍一邊躲避莎麗兒一邊襲擊人類，在莎麗兒趕到之前人類則負隅頑抗。
描繪出了這樣的一副構圖。